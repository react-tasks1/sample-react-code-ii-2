import React, { Component, lazy, Suspense } from "react";
import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import './App.css';
// import Sidebar from './components/Sidebar'
import { suspenseFallbackLoader } from './components/Loader'
const Sidebar = lazy(() => import("./components/Sidebar"));

export class App extends Component {
  render() {
    let load = suspenseFallbackLoader()
    return (
      <div className="App">
        <Router>
          <Suspense fallback={load}>
            <Switch>
              <Route exact path="/" render={(props) => <Sidebar {...props} />} />
            </Switch>
          </Suspense>
        </Router>
      </div>
    );
  }
}
export default App
