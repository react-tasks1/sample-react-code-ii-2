import { REHYDRATE } from "redux-persist";

let initState = {
  category_Data: [],
};

function taskReducer(state = initState, action) {
  switch (action.type) {
    case "CATEGORY_DATA":
      return {
        ...state,
        category_Data: action.res,
      };

    case REHYDRATE:
      return {
        ...state,
        category_Data:
          action.payload && action.payload.category_Data ? action.payload.category_Data : [],
      };
    default:
      return {
        ...state,
      };
  }
}

export const reducer = taskReducer;
