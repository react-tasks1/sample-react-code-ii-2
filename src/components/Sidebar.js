import React, { Component } from 'react'
import axios from "axios";
import '../App.css';
import List from "./List";

export class Sidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categoryData: [],
            selectID: 0,
        };
    }

    getCategory = () => {
        axios.get('https://api.thecatapi.com/v1/categories')
            .then(response => {
                if (response.data.length > 0) {
                    this.setState({ categoryData: response.data, selectID: response.data[0].id })
                }
            });
    };

    componentDidMount() {
        this.getCategory()
    }

    render() {
        const { categoryData, selectID, } = this.state
        return (
            <div className="container">
                <div className="Sidebar">
                    <ul className="SidebarList">
                        {categoryData.map((val) => {
                            return (
                                <li key={val.id}
                                    className="row"
                                    id={selectID == val.id ? "active" : ""}
                                    onClick={() => {
                                        this.setState({ selectID: val.id })
                                    }}
                                >
                                    <div id="title">
                                        {val.name}
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                </div>
                <div className="List">
                    <List data={selectID} />
                </div>
            </div>
        )
    }
}

export default Sidebar
