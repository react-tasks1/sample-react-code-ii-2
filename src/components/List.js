import React, { Component } from 'react'
import axios from "axios";

import { bindActionCreators } from "redux";
import { ActCreators } from "../redux/bindActionCreator";
import { connect } from "react-redux";
import '../App.css';


let category_Data = []

const mapStateToProps = (state) => {
    category_Data = state.category_Data;
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActCreators, dispatch);
};

export class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            singleCategoryData: [],
            loader: false,
            categoryId: 0,
        };
    }

    getDetails = (id) => {
        let oldData = this.state.singleCategoryData
        this.setState({ loader: true, categoryId: id })
        axios.get(`https://api.thecatapi.com/v1/images/search?limit=10&category_ids=${id}`)
            .then(response => {
                if (response.data.length > 0) {
                    response.data.map((value) => {
                        oldData.push(value)
                    })
                }
                this.props.CATEGORY_DATA(oldData)
                this.setState({ singleCategoryData: oldData, loader: false })
            });
    };
    componentDidMount() {
        this.getDetails(this.props.data)
    }

    componentDidUpdate(prevState) {
        if (prevState.data !== this.props.data) {
            this.setState({ singleCategoryData: [], },
                () => { this.getDetails(this.props.data) })

        }
    }

    render() {
        const { singleCategoryData, loader } = this.state
        return (
            <div>
                {loader === false && (
                    <>
                        <p>
                            {category_Data.length > 0 && singleCategoryData.map((val) => {
                                return (
                                    <img src={val.url} alt="" height="200px" width="200px" />
                                )
                            })
                            }
                        </p>
                        {category_Data.length > 0 && (
                            <button
                                className="button"
                                onClick={() => {
                                    this.getDetails(this.state.categoryId);
                                }}
                            >
                                {this.state.loader ? "  Fetching More..." : " More"}
                            </button>
                        )}
                    </>
                )}
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
